package com.mags.danzkemuel.calm;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

/**
 * Created by Andzro on 7/23/2016.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    private ImageView marker1;
    private ImageView marker2;
    private ImageView marker3;
    private ImageView marker4;
    private ImageView marker5;
    private Boolean marker1_enabled = false;
    private Boolean marker2_enabled = false;
    private Boolean marker3_enabled = false;
    private Boolean marker4_enabled = false;
    private Boolean marker5_enabled = false;

    private GoogleMap mMap;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_maps, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        marker1 = (ImageView)view.findViewById(R.id.imageView2);
        marker2 = (ImageView)view.findViewById(R.id.imageView3);
        marker3 = (ImageView)view.findViewById(R.id.imageView4);
        marker4 = (ImageView)view.findViewById(R.id.imageView6);
        marker5 = (ImageView)view.findViewById(R.id.imageView7);



        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        String url = ""
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        MarkerOptions options = new MarkerOptions().position(sydney).title("Marker in Sydney").icon(BitmapDescriptorFactory.defaultMarker());
//        mMap.addMarker(options);
//        mMap.addMarker(new MarkerOptions().position(new LatLng(-40, 151)).title("Marker in Test").icon(BitmapDescriptorFactory.fromResource(R.drawable.common_ic_googleplayservices)));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        MarkerListeners();

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(marker1_enabled) {
                    Toast.makeText(getContext(), latLng.toString(), Toast.LENGTH_LONG).show();
                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.earthquake)));
                    marker1_enabled = false;
                }
                else if(marker2_enabled) {
                    Toast.makeText(getContext(), latLng.toString(), Toast.LENGTH_LONG).show();
                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.eruption)));
                    marker2_enabled = false;
                }
                else if(marker3_enabled) {
                    Toast.makeText(getContext(), latLng.toString(), Toast.LENGTH_LONG).show();
                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.flood)));
                    marker3_enabled = false;
                }
                else if(marker4_enabled) {
                    Toast.makeText(getContext(), latLng.toString(), Toast.LENGTH_LONG).show();
                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.landslide)));
                    marker4_enabled = false;
                }
                else if(marker5_enabled) {
                    Toast.makeText(getContext(), latLng.toString(), Toast.LENGTH_LONG).show();
                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.typhoon)));
                    marker5_enabled = false;
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);




    }

//    public BitmapDescriptor MarkMode()
//    {
//        BitmapDescriptor custom_icon;
//        custom_icon = BitmapDescriptorFactory.fromResource(R.drawable.calmicon);
//        if(marker1_enabled||marker2_enabled||marker3_enabled||marker4_enabled||marker5_enabled)
//        {
//            if(marker1_enabled) {
//                custom_icon = BitmapDescriptorFactory.fromResource(R.drawable.earthquake);
//            }
//            else if(marker2_enabled) {
//                custom_icon = BitmapDescriptorFactory.fromResource(R.drawable.earthquake);
//            }
//            else if(marker3_enabled) {
//                custom_icon = BitmapDescriptorFactory.fromResource(R.drawable.earthquake);
//            }
//            else if(marker4_enabled) {
//                custom_icon = BitmapDescriptorFactory.fromResource(R.drawable.earthquake);
//            }
//            else if(marker5_enabled) {
//                custom_icon = BitmapDescriptorFactory.fromResource(R.drawable.earthquake);
//            }
//
//        }
//        return custom_icon;
//
//    }


    public void MarkerListeners()
    {

        marker1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Earthquake Marker Selected!",Toast.LENGTH_LONG).show();
                marker1_enabled = !marker1_enabled;
                marker2_enabled = false;
                marker3_enabled = false;
                marker4_enabled = false;
                marker5_enabled = false;

            }
        });

        marker2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Eruption Marker Selected!",Toast.LENGTH_LONG).show();
                marker2_enabled = !marker2_enabled;
                marker1_enabled = false;
                marker3_enabled = false;
                marker4_enabled = false;
                marker5_enabled = false;

            }
        });

        marker3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Flood Marker Selected!",Toast.LENGTH_LONG).show();
                marker3_enabled = !marker3_enabled;
                marker2_enabled = false;
                marker1_enabled = false;
                marker4_enabled = false;
                marker5_enabled = false;

            }
        });

        marker4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Landslide Marker Selected!",Toast.LENGTH_LONG).show();
                marker4_enabled = !marker4_enabled;
                marker2_enabled = false;
                marker3_enabled = false;
                marker1_enabled = false;
                marker5_enabled = false;

            }
        });

        marker5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Typhoon Marker Selected!",Toast.LENGTH_LONG).show();
                marker5_enabled = !marker5_enabled;
                marker2_enabled = false;
                marker3_enabled = false;
                marker4_enabled = false;
                marker1_enabled = false;

            }
        });
    }



}
